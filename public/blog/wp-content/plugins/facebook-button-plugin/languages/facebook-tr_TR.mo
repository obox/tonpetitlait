��    "      ,  /   <      �     �                    )     @     O     e     n     �  "   �     �     �     �     �          !  o   1  t   �  o     
   �     �     �     �  	   �     �     �     �  	             '  '   /     W  �  k     V     g     m     s     �     �     �     �  $   �      �  9   	     R	     m	     �	     �	     �	     �	  �   �	  �   �
  �   B     �     �     �          !     0     C     [     c     s     �  7   �     �     	                                   
                          !                                                                          "                  Activated plugins After Before Before and After Choose display option: Current image: Custom FaceBook image Download Error: File size > 32K Error: Invalid file type Error: check image width or height Error: moving file failed FAQ FaceBook Button FaceBook Button Options FaceBook Button Position: FaceBook image: If you have any questions, please contact us via plugin@bestwebsoft.com or fill in our contact form on our site If you would like to add a FaceBook button to your website, just copy and put this shortcode onto your post or page: Image properties: max image width:100px; max image height:40px; max image size:32Kb; image types:"jpg", "jpeg". Install %s Install now from wordpress.org Installed plugins Options saved. Read more Recommended plugins Save Changes Settings Shortcode Standart FaceBook image Support Uploading Error: check image properties Your's FaceBook Id: Project-Id-Version: facebook
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-07-24 19:37+0300
PO-Revision-Date: 2012-07-24 19:37+0300
Last-Translator: zos <zos@bestwebsoft.com>
Language-Team: bestwebsoft.com <plugin@bestwebsoft.com>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Poedit-Language: Spanish
X-Poedit-Country: SPAIN
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
 Aktif Eklentiler Sonra Önce Önce ve Sonra Gösterim Şeklini Seçin Şu andaki görsel: Kişisel Facebook Görseli İndir HATA: Dosya boyutu 32KB'tan büyük! HATA: geçersiz dosya uzantısı HATA: Görselin genişlik ve yüksekliğini kontrol edin. HATA: Dosya taşınamadı! SSS (Sık Sorulan Sorular) Facebook Butonu Facebook Butonu Ayarları Facebook Butonunun Konumu Facebook görseli: Sormak istediğiniz herhangi bir sorunuz varsa bize plugin@bestwebsoft.com adresine e-posta atarak ya da sitemizdeki iletişim formunu kullanarak ulaşabilirsiniz. Web sitenize Facebook butonu yerleştirmek istiyorsanız, aşağıdaki kodu kopyalayın ve sayfanızda/yazınızda butonun görünmesini istediğiniz yere yapıştırın.(HTML editörüyle!) Görsel Özellikleri: maksimum genişlik: 100px; maksimum yükseklik: 40px; maksimum boyut: 32KB; izin verilen uzantılar: "jpg", "jpeg". %s 'i yükle wordpress.org'dan yükle Yüklü eklentiler Değişiklikler Kaydedildi. Devamını Oku Eklenti Önerileri Değişiklikleri Kaydet Ayarlar Kısakod Olarak Standard Facebook Görseli Destek Yükleme Hatası: Görselin özelliklerini kontol edin. Facebook ID: 