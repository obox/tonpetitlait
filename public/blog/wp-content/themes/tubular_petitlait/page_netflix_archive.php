<?php
/*
Template Name: Netflix Archive
*/
?>

<?php get_header(); ?>

<div id="content">

    <div id="homepage">
                                    
      <div id="hpbottom">
        
			 <?php $page = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts("cat=347&showposts=10&offset=10"); while ( have_posts() ) : the_post() ?>

			<div class="pagevideos">
			<div class="entryfeatured">
			
                <div class="videosleft">
					 <?php echo get_post_meta($post->ID, "_video_description", true); ?>
                </div>
                
                  <div class="videosrightmini">
                   <script type="text/javascript">
tweetmeme_style = 'compact';
</script>
<script type="text/javascript">tweetmeme_url = '<?php the_permalink(); ?>';</script>
<script type="text/javascript" src="http://tweetmeme.com/i/scripts/button.js"></script>
                </div>
                
                <div id="videosrighttitle">
                    <h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>
                    <p class="date">Posted by <?php the_author_posts_link(); ?> on <?php the_time('d.m.y'); ?>&nbsp;<?php edit_post_link('(Edit)', '', ''); ?></p>
                     

                </div>
                 
         
          <div class="videosrightbox">
              
                     <p class="videosrightbox"> <?php the_content_limit(160, "[Lire la suite] "); ?></p>    

                </div>
                <div style="clear:both;"></div>
                 
			</div>
            <div class="commentright">
 <p class="postdate"><a href="<?php the_permalink(); ?>#respond"><?php comments_number('Leave a Comment', '1 Comment', '% Comments'); ?></a> </p> </div> 

               <div class="postmeta">
                <p class="postdate">Tags : <?php the_tags('') ?></p>
            </div>
			</div>
             
			<?php endwhile; ?>
            
            <div class="nextpage">
               <p class="nextpage"><?php if(function_exists('wp_page_numbers')) : wp_page_numbers(); endif; ?></p>
            </div>	

                        
    </div>
    
	</div>
    
<?php include(TEMPLATEPATH."/sidebar.php");?>
	
</div>



<?php get_footer(); ?>