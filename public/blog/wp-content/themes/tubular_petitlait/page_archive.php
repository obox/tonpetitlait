<?php
/*
Template Name: Archive
*/
?>
<?php get_header(); ?>
<div id="content">
<div id="homepage">

<div id="contentleft">
     <div class="postarea">
         <div class="entry">
            <h1>Archives du site</h1>
	<div class="archive">
		<h4>Par Pages :</h4>
			<ul><?php wp_list_pages('title_li='); ?></ul>
				
		<h4>Par Mois :</h4>
			<ul><?php wp_get_archives('type=monthly'); ?></ul>
							
		<h4>Par Catégories :</h4>
			<ul><?php wp_list_categories('sort_column=name&title_li='); ?></ul>
		</div>
<div class="archive">
		<h4>Par Posts :</h4>
			<ul><?php wp_get_archives('type=postbypost&limit=100'); ?> </ul>
			</div>
                <div style="clear:both;"></div>
</div></div> </div></div>
<?php include(TEMPLATEPATH."/sidebar.php");?>
</div>
<!-- The main column ends  -->
<?php get_footer(); ?>