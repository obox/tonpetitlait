<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb=”http://www.facebook.com/2008/fbml” <?php language_attributes('xhtml'); ?>>
<head profile="http://gmpg.org/xfn/11">

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="distribution" content="global" />
<meta name="language" content="en" />

<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
<link rel="Shortcut Icon" href="<?php echo bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="<?php echo bloginfo('template_url'); ?>/images/animated_favicon1.gif" >

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
<?php wp_head(); ?>

<script type="text/javascript"><!--//--><![CDATA[//><!--
sfHover = function() {
	if (!document.getElementsByTagName) return false;
	var sfEls = document.getElementById("nav").getElementsByTagName("li");

	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
//--><!]]></script>

<!--
   Copy and paste this embed code in the HEAD section of every HTML page
   you would like to have include one or more AdGear Ad Spots:
-->
<script type="text/javascript" language="JavaScript">
/*
<![CDATA[
*/
(function() {
  var proto = "http:";
  var host = "cdn.adgear.com";
  var bucket = "a";
  if (window.location.protocol == "https:") {
    proto = "https:";
    host = "a.adgear.com";
    bucket = "";
  }
  document.writeln('<scr' + 'ipt type="text/ja' + 'vascr' + 'ipt" s' + 'rc="' +
      proto + '//' + host + '/' + bucket + '/adgear.js/current/adgear.js' +
      '"></scr' + 'ipt>');
})();
/*
]]>
*/
</script>
<script type="text/javascript" language="JavaScript">
/*
<![CDATA[
*/
  ADGEAR.tags.script.init();
  ADGEAR.lang.namespace("ADGEAR.site_callbacks");
  ADGEAR.site_callbacks.variables = function() {
    return { };
  }
/*
]]>
*/
</script>

</head>

<body>
<?php if ($_SERVER["REQUEST_URI"] == '/') {?>
 
 <?php } else {?> <?php } ?>
 <div id="wrap">
<div style="clear:both;"></div>

<div id="header">
    <div class="headerleft">
		<?php $tubular_header = get_option('tubular_header'); ?>
        <?php if($tubular_header == 'Text'): ?>
        <h1><a href="<?php echo get_settings('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
        <p><?php bloginfo('description'); ?></p>    
        <?php else : ?>


        <a href="<?php echo get_option('home'); ?>/"><img src="<?php echo bloginfo('url'); ?>/blog/tiles/logo-01.jpg" alt="<?php bloginfo('name'); ?>" /></a>


        <?php endif; ?>	
	</div>
    
	<div class="headerright">
		<div class="headertop">
	<span class="adgear">
		<script type="text/javascript" language="JavaScript">
		/*
		<![CDATA[
		*/
		  ADGEAR.tags.script.universal({
			  "chip_key":  "547424901950012f82810024e87a30c2",
			  "container_id": "1841",
			  "format_id": "2", /* IAB - Leaderboard - (728x90) */
			  "path":     ["Header"]
			});
		/*
		]]>
		*/
		</script>
	</span>
</div>
</div>
<div style="clear:both;"></div>

	<div id="navbar">
<div class="navbarright">
		<form id="searchform" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		<input type="text" value="Recherche..." name="s" id="searchbox" onfocus="if (this.value == 'Recherche...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Recherche...';}" />
		<input type="submit" id="searchbutton" value="GO" /></form>
</div>

	<ul id="nav">
		<li><a href="<?php echo get_option('home'); ?>">Accueil</a></li>
		<?php wp_list_pages('title_li=&depth=4&sort_column=menu_order&exclude=4,5'); ?>
	</ul>
</div>
<div style="clear:both;"></div>
</div>      
<div style="clear:both;"></div>
