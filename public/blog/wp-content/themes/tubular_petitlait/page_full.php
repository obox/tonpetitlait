<?php
/*
Template Name: Full Width
*/
?>

<?php get_header(); ?>

<div id="content">

	<div id="contentfull">
    
        <div class="postarea">
        
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        
       		<div class="entry">
            
                <h1><?php the_title(); ?></h1>
            
                <?php the_content(__('Lire la suite'));?><div style="clear:both;"></div>
                <p><?php edit_post_link('(Edit)', '', ''); ?></p>
                <?php endwhile; else: ?>
                
                <p><?php _e('Désolé, aucun post ne correspond à votre recherche.'); ?></p><?php endif; ?>
                
            </div>
            
        </div>
	
	</div>
		
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>