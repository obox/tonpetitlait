<?php get_header(); ?>

<div id="content">
<div id="homepage">

	<div id="contentleft">
    
     	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
        <div class="postarea">
<div class="entry">
<div class="videorightpostarea">
<script type="text/javascript">tweetmeme_url = '<?php the_permalink(); ?>';</script>
<script type="text/javascript" src="http://tweetmeme.com/i/scripts/button.js"></script>
                </div>        
    
            <h1><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                
                <div class="postdate">
                    <p class="postdate">Posted by <?php the_author_posts_link(); ?> on <?php the_time('d.m.y'); ?> &nbsp;<?php edit_post_link('(Edit)', '', ''); ?></p>
                </div>
 <?php the_content(__('Lire la suite'));?><div style="clear:both;"></div>
</div>
                        
          <div class="commentright">
 <p class="postdate"><a href="<?php the_permalink(); ?>#respond"><?php comments_number('Leave a Comment', '1 Comment', '% Comments'); ?></a> </p> </div> 
                 <div class="postmeta">
                <p class="postdate">Tags : <?php the_tags('') ?>&nbsp;&nbsp; </p> </div> 
 </div>
                
        <?php endwhile; else: ?>
<p class="nextpage"><?php _e('Désolé, aucun post ne correspond à votre recherche.'); ?></p><?php endif; ?>
       <div class="nextpage">
       <p class="nextpage"><?php if(function_exists('wp_page_numbers')) : wp_page_numbers(); endif; ?></p>
            </div>
</div></div>
	
<?php include(TEMPLATEPATH."/sidebar.php");?>
	
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>