Tubular WordPress Theme
http://www.studiopress.com/themes/tubular

INSTALL: 
1. Upload the theme folder via FTP to your wp-content/themes/ directory.
2. Go to your WordPress dashboard and select Appearance.
3. Select Tubular WordPress theme.
4. To configure the homepage, go to your WordPress dashboard under Appearance > Tubular Theme Options and configure to your liking.
5. In order to get the drop down menus in front of the videos, you need to add wmode="transparent" to the embedded video code you use in your post. An example of this would look like:

<embed wmode="transparent" src="http://vimeo.com/moogaloop.swf?clip_id=1637101.......

FONT:
If you want to know what font was used in the logo of the theme demo, it was Space Age

If you are looking for theme support, please visit http://www.studiopress.com/support

Brian Gardner
brian@briangardner.com
http://www.briangardner.com