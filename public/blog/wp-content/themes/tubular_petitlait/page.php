<?php get_header(); ?>

<div id="content">
<div id="homepage">

	<div id="contentleft">
    
        <div class="postarea">
        
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
       		<div class="entry">
            
                <h1><?php the_title(); ?></h1>
            <p><?php edit_post_link('(Edit)', '', ''); ?></p>
                <?php the_content(__('Lire la suite'));?><div style="clear:both;"></div>
                
                <?php endwhile; else: ?>
                
                <p><?php _e('Désolé, aucun post ne correspond à ta recherche.'); ?></p><?php endif; ?>
                
            </div>
            
        </div>
        </div>
	
	</div>
	
<?php include(TEMPLATEPATH."/sidebar.php");?>
	
</div>

<!-- The main column ends  -->

<?php get_footer(); ?>